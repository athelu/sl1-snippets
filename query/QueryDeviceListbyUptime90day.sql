#get devices with uptime over 90 days
	
SELECT device, DATE(DATE_SUB(snmp.last_poll, INTERVAL snmp.sysuptime/100 SECOND)) as lastreboot
	FROM master_biz.organizations org
	JOIN master_dev.legend_device dev ON (org.roa_id = dev.roa_id)
	LEFT JOIN master_dev.device_snmp_data snmp ON dev.id=snmp.did
    where DATE(DATE_SUB(snmp.last_poll, INTERVAL snmp.sysuptime/100 SECOND)) < DATE_SUB(Date(Now()), INTERVAL 90 DAY)