# SL1-Snippets

Repository for quick snippets

## Structure of the repository

This repository is structured by programming language
| Folder         | Description                                               |
| -------------- | --------------------------------------------------------- |
| bash           | Contains useable scripts for sl1 devices                  |
| PHP            | Contains useable scripts for php                          |
| PHP\Report     | contains php scripts for SL1 Reporting                    |
| Python         | Contains useable files for Python                         |
| Python\DynApp  | Contains snippets useable in SL1 Dynamic Application      |
| Python\RunBook | Contains snippets useable in SL1 Runbooks                 |
| Query          | Contains queries for SL1 Database structure               |