#F find ALERTS created during specific Time and with select severity

select ec.date_first,ec.date_last, ec.xname, ec.id, ld.cug_id, ec.emessage, ec.eseverity
	from master_events.events_cleared ec
	join master_dev.legend_device ld
	on ec.xid = ld.id
where ec.date_first >= '2018-10-02 23:00' and ec.date_first <= '2018-10-3 12:00' and ec.eseverity >= 3 